#![feature(decl_macro)]
///////////
// lints //
///////////
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required
// during release #![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet to come
#![allow(clippy::missing_panics_doc)]
#![allow(clippy::missing_errors_doc)]

mod reactive;
mod view;

use crate::{
	reactive::{rx::Rx, tx::Tx},
	view::{model::Element, TryInto},
};
use anyhow::{anyhow, Result};
use log::{info, Level};
use std::{
	rc::Rc,
	sync::atomic::{AtomicU32, Ordering},
};
use wasm_bindgen::{prelude::wasm_bindgen, JsValue};

#[wasm_bindgen]
pub fn main() -> Result<(), JsValue> {
	main_result().map_err(|err| err.to_string().into())
}

pub fn main_result() -> Result<()> {
	console_log::init_with_level(Level::Debug).unwrap();

	let window = web_sys::window().expect("no global `window` exists");
	let document = window.document().expect("should have a document on window");

	let counter_text = Rc::new(Rx::new());

	let counter_value = AtomicU32::new(0);
	let counter = counter_text
		.clone()
		.map(|number| format!("counter: {}", number));

	let root_view = Element::new("main")
		.attribute("class", "root".to_string())
		.child(Element::new("h1").child("hello world!"))
		.child(
			Element::new("button")
				.child("increment")
				.on_click(move |_| counter.send(counter_value.fetch_add(1, Ordering::Relaxed) + 1)),
		)
		.child(("the counter will appear here!".to_string(), counter_text))
		.into_view(&document)?;

	root_view.append_leaking(
		&document
			.body()
			.ok_or_else(|| anyhow!("failed to acquire a reference to the document body"))?,
	)?;

	info!("hi world");

	Ok(())
}
