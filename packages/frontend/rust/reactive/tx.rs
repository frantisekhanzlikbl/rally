use std::{marker::PhantomData, rc::Rc};

use super::map::Map;

pub trait Tx {
	type Item;
	fn send(&self, value: Self::Item);

	fn map<U, TMap>(self: Rc<Self>, map: TMap) -> Map<TMap, Self, U>
	where
		Self: Sized,
		TMap: Fn(U) -> Self::Item,
	{
		Map {
			map,
			tx: self,
			ph_t: PhantomData,
		}
	}
}

#[cfg(test)]
mod tests {
	use std::{rc::Rc, sync::Mutex};

	use super::Tx;

	struct Collector<'a, T> {
		target: &'a Mutex<Vec<T>>,
	}
	impl<T> Tx for Collector<'_, T> {
		type Item = T;

		fn send(&self, value: Self::Item) {
			self.target.lock().unwrap().push(value);
		}
	}

	#[test]
	fn send() {
		let messages = Mutex::new(Vec::new());
		let c = Collector { target: &messages };

		c.send("some string".to_string());
		assert_eq!(&*messages.lock().unwrap(), &["some string".to_string()]);
	}
	#[test]
	fn send_multiple() {
		let messages = Mutex::new(Vec::new());
		let c = Collector { target: &messages };

		c.send("one");
		c.send("two");
		assert_eq!(&*messages.lock().unwrap(), &["one", "two"]);
	}
	#[test]
	fn map() {
		let messages = Mutex::new(Vec::new());
		let c = Rc::new(Collector { target: &messages });

		c.map(|s: &str| format!("hello {}", s)).send("world");
		assert_eq!(&*messages.lock().unwrap(), &["hello world".to_string()]);
	}
}
