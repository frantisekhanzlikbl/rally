use std::marker::PhantomData;

use super::tx::Tx;

pub struct Fun<T, Fn> {
	ph_t: PhantomData<T>,
	fun: Fn,
}

impl<T, F> Tx for Fun<T, F>
where
	F: Fn(T),
{
	type Item = T;

	fn send(&self, value: Self::Item) {
		(self.fun)(value)
	}
}

impl<T, F> From<F> for Fun<T, F>
where
	F: Fn(T),
{
	fn from(fun: F) -> Self {
		Self {
			fun,
			ph_t: PhantomData,
		}
	}
}

impl<'a, T, F> From<F> for Box<dyn Tx<Item = T> + 'a>
where
	F: Fn(T) + 'a,
	T: 'a,
{
	fn from(fun: F) -> Self {
		Box::new(Fun::from(fun))
	}
}
