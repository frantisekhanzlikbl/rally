use std::{marker::PhantomData, rc::Rc};

use super::tx::Tx;

pub struct Map<MapFn, NextTx, T> {
	pub(super) ph_t: PhantomData<T>,
	pub(super) map: MapFn,
	pub(super) tx: Rc<NextTx>,
}

impl<MapFn, NextTx, T, U> Tx for Map<MapFn, NextTx, T>
where
	NextTx: Tx<Item = U>,
	MapFn: Fn(T) -> U,
{
	type Item = T;

	fn send(&self, value: Self::Item) {
		self.tx.send((self.map)(value));
	}
}
