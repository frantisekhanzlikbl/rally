use super::value::Value;
use crate::{
	reactive::rx::Rx,
	view::{self, View},
};
use anyhow::Result;
use std::{rc::Rc, string::String};
use web_sys::Document;

pub struct Text {
	value: Value<String>,
}

impl From<&str> for Text {
	fn from(value: &str) -> Self {
		value.to_string().into()
	}
}

impl From<String> for Text {
	fn from(value: String) -> Self {
		Self {
			value: value.into(),
		}
	}
}

impl From<Rc<Rx<String>>> for Text {
	fn from(rx: Rc<Rx<String>>) -> Self {
		Self { value: rx.into() }
	}
}

impl From<(String, Rc<Rx<String>>)> for Text {
	fn from(tuple: (String, Rc<Rx<String>>)) -> Self {
		Self {
			value: tuple.into(),
		}
	}
}

impl view::TryInto for Text {
	fn into_view(self, document: &Document) -> Result<View> {
		let initial_value = match &self.value {
			Value::Static(value) => value,
			Value::Dynamic { .. } => "",
			Value::DynamicWithInitial { initial, .. } => initial,
		};

		let text = document.create_text_node(initial_value);

		match self.value {
			Value::Static(_) => {},
			Value::Dynamic { rx } | Value::DynamicWithInitial { rx, .. } => {
				rx.set_handler(Box::new({
					let text = text.clone();
					move |value| text.set_text_content(Some(&value))
				}));
			},
		}

		Ok(View {
			node: text.into(),
			_children: Vec::new(),
		})
	}
}
