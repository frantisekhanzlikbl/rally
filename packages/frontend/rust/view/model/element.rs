use super::value::Value;
use crate::{
	reactive::tx::Tx,
	view::{self, View},
};
use anyhow::{anyhow, Result};
use js_sys::Function;
use paste::paste;
use std::collections::HashMap;
use wasm_bindgen::{prelude::Closure, JsCast};
use web_sys::{Document, MouseEvent};

type Handler<T> = Box<dyn Tx<Item = T>>;

macro_rules! impl_event_handler_inserters {
	($struct_name:ident; { $($name:ident : $type:ty),* }) => {
		paste! {
			impl $struct_name {
				$(
					pub fn [<on_ $name>](mut self, handler: impl Into<Handler<$type>> ) -> Self {
						let handler = handler.into();

						self.event_handlers.push((
							stringify!($name),
							Closure::wrap(Box::new(move |event| {
								handler.send(event);
							}) as Box<dyn Fn($type)>).into_js_value().unchecked_into()
						));

						self
					}
				)*
			}
		}
	}
}

pub struct Element {
	tag_name: String,
	attributes: HashMap<String, Value<String>>,
	children: Vec<view::Model>,
	event_handlers: Vec<(&'static str, Function)>,
}

impl_event_handler_inserters!(Element; {click: MouseEvent});

impl Element {
	pub fn new(tag_name: impl Into<String>) -> Self {
		Self {
			tag_name: tag_name.into(),
			attributes: HashMap::new(),
			children: Vec::new(),
			event_handlers: Vec::new(),
		}
	}

	pub fn child(mut self, child: impl Into<view::Model>) -> Self {
		self.children.push(child.into());
		self
	}

	pub fn attribute(mut self, key: impl Into<String>, value: impl Into<Value<String>>) -> Self {
		self.attributes.insert(key.into(), value.into());
		self
	}
}

impl view::TryInto for Element {
	fn into_view(self, document: &Document) -> Result<View> {
		let element = document
			.create_element(&self.tag_name)
			.map_err(|js_val| anyhow!("failed to create element. js error: {:?}", js_val))?;

		for (name, value) in self.attributes {
			// set the initial value
			match &value {
				Value::Static(value) | Value::DynamicWithInitial { initial: value, .. } => {
					element.set_attribute(&name, value).map_err(|js_val| {
						anyhow!("failed to set attribute. js error: {:?}", js_val)
					})?;
				},
				Value::Dynamic { .. } => {},
			};

			match value {
				Value::Dynamic { rx } | Value::DynamicWithInitial { rx, .. } => {
					rx.set_handler(Box::new({
						let element = element.clone();
						move |value| {
							element
								.set_attribute(&name, &value)
								.map_err(|js_val| {
									anyhow!("failed to set attribute. js error: {:?}", js_val)
								})
								.unwrap()
						}
					}))
				},
				Value::Static(_) => {},
			}
		}

		for (name, handler) in self.event_handlers {
			element
				.add_event_listener_with_callback(name, &handler)
				.unwrap();
		}

		let child_views = self
			.children
			.into_iter()
			.map(|child| -> Result<View> {
				let child_view = child.into_view(document)?;

				child_view.append(&element)?;

				Ok(child_view)
			})
			.collect::<Result<Vec<_>>>()?;

		Ok(View {
			node: element.into(),
			_children: child_views,
		})
	}
}
