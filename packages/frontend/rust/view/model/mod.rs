mod element;
mod text;
mod value;

pub use element::Element;
pub use text::Text;

use crate::{reactive::rx::Rx, view};
use anyhow::Result;
use std::rc::Rc;
use view::View;
use web_sys::Document;

pub enum Model {
	Element(Element),
	Text(Text),
}

impl view::TryInto for Model {
	fn into_view(self, document: &Document) -> Result<View> {
		match self {
			Model::Element(inner) => inner.into_view(document),
			Model::Text(inner) => inner.into_view(document),
		}
	}
}

impl From<Element> for Model {
	fn from(element: Element) -> Self {
		Self::Element(element)
	}
}

impl From<Text> for Model {
	fn from(text: Text) -> Self {
		Self::Text(text)
	}
}

impl From<String> for Model {
	fn from(value: String) -> Self {
		Self::Text(Text::from(value))
	}
}

impl From<&str> for Model {
	fn from(value: &str) -> Self {
		value.to_string().into()
	}
}

impl From<Rc<Rx<String>>> for Model {
	fn from(rx: Rc<Rx<String>>) -> Self {
		Text::from(rx).into()
	}
}

impl From<(String, Rc<Rx<String>>)> for Model {
	fn from(tuple: (String, Rc<Rx<String>>)) -> Self {
		Text::from(tuple).into()
	}
}
