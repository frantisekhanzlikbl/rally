pub mod model;

use std::mem::forget;

use anyhow::{anyhow, Result};
pub use model::Model;

use web_sys::{Document, Node};

pub struct View {
	node: Node,
	_children: Vec<View>,
}

impl View {
	pub fn append(&self, parent: &impl AsRef<Node>) -> Result<()> {
		parent.as_ref().append_child(&self.node).map_err(|js_err| {
			anyhow!("failed to append a view as a child. js error: {:?}", js_err)
		})?;

		Ok(())
	}

	pub fn append_leaking(self, parent: &impl AsRef<Node>) -> Result<()> {
		self.append(parent)?;

		forget(self);

		Ok(())
	}
}

pub trait TryInto {
	fn into_view(self, document: &Document) -> Result<View>;
}
