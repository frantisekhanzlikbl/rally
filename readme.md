# A Performant Reactive Web Framework

Azazel strives to be a next-generation web framework,
which resulted from my unsatisfaction with the status quo of the web platform and available technology.

# Goals

The aim is for Azazel to comply with all the following:

## Be easy to get right

This is hard to stress enough - In most of the existing web frameworks,
it is really hard for me to be confident in the code I write,
because often I find that there is an immense amount of unintuitive guidelines that one has to adhere to
for his code to be easy to maintain and performant.

I want Azazel to be intuitive and easy to use the "right way"

## Be friendly to the developer

This consists of two points:

 *	Use a language that is easier to read than HTML, but just as expressive as JSX.
 *	Write the application logic in a language which is harder to misuse than JavaScript.
  	No more hours of debugging mismatched types.

## Be friendly to the user

This means that it should be easy to make the resulting applications accessible,
but also to give the user a freedom to disable undesirable technologies such as JavaScript and cookies while not sacrificing the core functionality.

# Chosen solutions

To accomplish the above goals, the following solutions were chosen:

## Reactivity and no Virtual DOM

This is the major difference from the most commonly used frameworks,
such as [React](https://reactjs.org/), [Angular](https://angular.io/) or [Vue.js](https://vuejs.org/)

The core idea of frameworks with virtual DOM is that on a change to the application state you get passed the new state
and generate a new version of your application based on it.
The framework then diffs this new version against the old version to dispatch the necessary updates to the real DOM.

The problem with this approach is that the diffing phase is very costly and that in some scenarios the whole application structure needs to be held in the memory twice.

Instead, Azazel chooses to continue in the direction of "reactivity" taken by for example [Svelte](https://svelte.dev/) and [Solid](https://www.solidjs.com/).

With reactivity, the application only performs the construction of the whole structure once on the application startup,
and reacts to changes by directly mapping them to updates on the real DOM elements.

## Rust

The language of choice for writing both the application logic and the framework is Rust,
a "blazingly fast and memory-efficient" language that makes it easy to write reliable software that runst exceptionally fast.

## Server side rendering

// TODO

